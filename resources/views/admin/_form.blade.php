<div class="card ">

    {!! Form::open(['method' => $method, 'url' => $url]) !!}

    @if ($errors->any())
        <div class="alert alert-danger" style="margin: 20px">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card-body">

        {{ $input }}

        @if ($submit === 'Create')
            <input type="submit" class="btn btn-primary" value="Create">
        @else
            <input type="submit" class="btn btn-primary" value="Edit">
        @endif

    </div>

    {!! Form::close() !!}
</div>

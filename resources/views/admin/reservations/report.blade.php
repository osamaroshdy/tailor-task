@extends('layouts.back-end.app', ['model' => 'Reservation'])

@section('content')

    <div class="card card-solid">
        <div class="card-header">
            <h3 class="card-title">Reservations Graph</h3>
        </div>
        <div class="card-body border-radius-none">
            <div class="chart" id="line-chart" style="height: 250px;"></div>
        </div>
        <!-- /.box-body -->
    </div>


@endsection

@push('js')

    <script>

        //line chart
        var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: [
                @foreach ($reservations as $data)
                {
                    ymd: "{{ $data->year }}-{{ $data->month }}-{{ $data->day }}",
                    count: "{{ $data->count }}"
                },
                @endforeach
            ],
            xkey: 'ymd',
            ykeys: ['count'],
            labels: ['Cont'],
            lineWidth: 2,
            hideHover: 'auto',
            gridStrokeWidth: 0.4,
            pointSize: 4,
            gridTextFamily: 'Open Sans',
            gridTextSize: 20,
        });


    </script>

@endpush

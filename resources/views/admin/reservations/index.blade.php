@extends('layouts.back-end.app', ['model' => 'Reservation'])

@section('content')

    <div class="card">
        <div class="card-header with-border">

            <a class="btn btn-sm btn-success" href="{{ aurl('reservation/accepted') }}">Reservations Accepted</a>
            <a class="btn btn-sm btn-danger" href="{{ aurl('reservation/rejected') }}">Reservations Rejected</a>
            <a class="btn btn-sm btn-default" href="{{ aurl('reservation') }}">Reservations ِAll</a>

            <p class="float-right btn btn-default">{{ $reservations->count() }} of {{ $reservations->total() }}</p>

        </div><!-- end of box header -->

        <div class="card-body">

            @if ($reservations->count() > 0)

                <table class="table table-hover">

                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Doctor</th>
                        <th>User</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($reservations as $index=>$reservation)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td><a href="{{ aurl('doctor/' . $reservation->doctor->id) }}">{{ $reservation->doctor->name }}</a></td>
                            <td><a href="{{ aurl('user/' . $reservation->user->id) }}">{{ $reservation->user->name }}</a></td>
                            <td>{{ $reservation->status }}</td>
                            <td>{{ $reservation->created_at->format('Y-m-d') }}</td>
                        </tr>

                    @endforeach
                    </tbody>

                </table><!-- end of table -->




            @else

                <h2>No Data Found</h2>

            @endif

        </div><!-- end of box body -->

        <div class="card-footer">
            {{ $reservations->links() }}

        </div>


    </div><!-- end of box -->

@endsection

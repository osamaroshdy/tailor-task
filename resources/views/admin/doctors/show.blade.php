@extends('layouts.back-end.app', ['model' => 'Doctor'])

@section('content')

    <div class="card">

        <div class="card-body">

                <div class="row">
                    <div class="col-sm-2">Status : </div>
                    <div class="col-sm-10">
                        {{ $doctor->status }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Name : </div>
                    <div class="col-sm-10">
                        {{ $doctor->name }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Specialty : </div>
                    <div class="col-sm-10">
                        {{ $doctor->specialty }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Email : </div>
                    <div class="col-sm-10">
                        {{ $doctor->email }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Fees : </div>
                    <div class="col-sm-10">
                        {{ $doctor->fees }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Location : </div>
                    <div class="col-sm-10">
                        {{ $doctor->location }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Description : </div>
                    <div class="col-sm-10">
                        {{ $doctor->description }}
                    </div>
                </div>

        </div><!-- end of box body -->
        <h2 class="text-center">Reservations</h2>
        <div class="card container">
            <table class="table table-hover">

                <thead>
                <tr>
                    <th>#</th>
                    <th>User</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($doctor->reservations as $index=>$reservation)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $reservation->user->name }}</td>
                        <td>{{ $reservation->status }}</td>
                        <td>{{ $reservation->created_at->format('Y-M-D') }}</td>
                @endforeach
                </tbody>

            </table><!-- end of table -->
        </div>

    </div><!-- end of box -->

@endsection

@extends('layouts.back-end.app', ['model' => 'Doctor'])

@section('content')

    <div class="card">

        <div class="card-header with-border">

            <form action="{{ aurl('doctor/index') }}" method="get">

                <div class="row">

                    <div class="col-md-4">
                        <input type="text" name="search" class="form-control" placeholder="search" value="{{ request()->search }}">
                    </div>

                    <div class="col-md-4">
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
                    </div>

                </div>
            </form><!-- end of form -->

        </div><!-- end of box header -->

        <div class="card-body">

            @if ($doctors->count() > 0)

                <table class="table table-hover">

                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Specialty</th>
                        <th>Fees</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($doctors as $index=>$doctor)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $doctor->name }}</td>
                            <td>{{ $doctor->specialty }}</td>
                            <td>{{ $doctor->fees }}</td>
                            <td><p class="btn btn-sm {{ $doctor->status === 'Pending' ? 'btn-warning' : 'btn-success' }} ">{{ $doctor->status }}</p></td>
                            <td>
                                <a href="{{ aurl('doctor/'. $doctor->id ) }}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Show</a>
                                <form action="{{ route('dashboard.doctor.destroy', $doctor->id) }}" method="post" style="display: inline-block">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-danger delete btn-sm"><i class="fa fa-trash"></i> Delete</button>
                                </form><!-- end of form -->
                                <a href="{{ aurl('doctor/'. $doctor->id . '/edit') }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> {{ $doctor->status === 'Pending' ? 'Approved' : 'Pending' }} Doctor</a>

                            </td>
                        </tr>

                    @endforeach
                    </tbody>

                </table><!-- end of table -->

                {{ $doctors->appends(request()->query())->links() }}

            @else

                <h2>No Data Found</h2>

            @endif

        </div><!-- end of box body -->


    </div><!-- end of box -->

@endsection

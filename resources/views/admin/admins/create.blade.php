@extends('layouts.back-end.app', ['model' => 'Admin'])

@section('content')

    @component('admin._form', ['method' => 'POST', 'url' => aurl('admin') , 'submit' => 'Create'])

        @slot('input')

            <div class="form-group">
                <label>Name</label>
                {!! Form::text('name',old('name'), ['class'=> 'form-control']) !!}
            </div>

            <div class="form-group">
                <label>Email</label>
                {!! Form::text('email',old('email'), ['class'=> 'form-control']) !!}
            </div>

            <div class="form-group">
                <label>Password</label>
                {!! Form::password('password', ['class'=> 'form-control']) !!}
            </div>

            <div class="form-group">
                <label>Password Confirmation</label>
                {!! Form::password('password_confirmation', ['class'=> 'form-control']) !!}
            </div>

        @endslot

    @endcomponent

@endsection

















@extends('layouts.back-end.app', ['model' => 'Admin'])

@section('content')

    <div class="card">

        <div class="card-header with-border">

            <form action="{{ aurl('admin/index') }}" method="get">

                <div class="row">

                    <div class="col-md-4">
                        <input type="text" name="search" class="form-control" placeholder="search" value="{{ request()->search }}">
                    </div>

                    <div class="col-md-4">
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
                        <a href="{{ aurl('admin/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create </a>

                    </div>

                </div>
            </form><!-- end of form -->

        </div><!-- end of box header -->

        <div class="card-body">

            @if ($admins->count() > 0)

                <table class="table table-hover">

                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($admins as $index=>$admin)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $admin->name }}</td>
                            <td>{{ $admin->email }}</td>
                            <td>
                                <a href="{{ aurl('admin/'. $admin->id . '/edit') }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                <form action="{{ route('dashboard.admin.destroy', $admin->id) }}" method="post" style="display: inline-block">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-danger delete btn-sm"><i class="fa fa-trash"></i> Delete</button>
                                </form><!-- end of form -->
                            </td>
                        </tr>

                    @endforeach
                    </tbody>

                </table><!-- end of table -->

                {{ $admins->appends(request()->query())->links() }}

            @else

                <h2>No Data Found</h2>

            @endif

        </div><!-- end of box body -->


    </div><!-- end of box -->

@endsection

@extends('layouts.back-end.app', ['model' => 'Admin'])

@section('content')

    @component('admin._form', ['method' => 'PUT', 'url' => aurl('admin/' . $admin->id)  , 'submit' => 'Edit'])

        @slot('input')
            <div class="form-group">
                {!! Form::label('Name') !!}
                {!! Form::text('name',$admin->name, ['class'=> 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Email') !!}
                {!! Form::email('email',$admin->email, ['class'=> 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Password') !!}
                {!! Form::password('password', ['class'=> 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Password Confirmation') !!}
                {!! Form::password('password_confirmation', ['class'=> 'form-control']) !!}
            </div>

        @endslot

    @endcomponent

@endsection


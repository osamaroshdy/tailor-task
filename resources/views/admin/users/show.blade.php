@extends('layouts.back-end.app', ['model' => 'User'])

@section('content')

    <div class="card">

        <div class="card-body">

            <div class="row">
                <div class="col-sm-2">Name : </div>
                <div class="col-sm-10">
                    {{ $user->name }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">Email : </div>
                <div class="col-sm-10">
                    {{ $user->email }}
                </div>
            </div>


        </div><!-- end of box body -->
        <h2 class="text-center">Reservations</h2>
        <div class="card container">
            <table class="table table-hover">

                <thead>
                <tr>
                    <th>#</th>
                    <th>Doctor</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($user->reservations as $index=>$reservation)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $reservation->doctor->name }}</td>
                        <td>{{ $reservation->status }}</td>
                        <td>{{ $reservation->created_at->format('Y-M-D') }}</td>
                @endforeach
                </tbody>

            </table><!-- end of table -->
        </div>

    </div><!-- end of box -->

@endsection

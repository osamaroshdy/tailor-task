@extends('layouts.doctors.app')

@section('content')
    <div class="container">
        <h1>Reservations</h1>
        <div class="card">
            <div class="card-body">
                {!! Form::open(['url' => url('doctor/reservation/'. $reservation->id), 'method' => 'put']) !!}

                <div class="form-group">
                    {!! Form::select('placement', ['1' => 'Accept', '0' => 'Reject'], old('placement') ,['class' => 'form-control', 'placeholder' => 'Select Status']) !!}
                </div>
                    <div class="card form-control">User : {{$reservation->user->name}}</div>
                <br>
                    <div class="card form-control">Date : {{$reservation->created_at}}</div>
                <br>
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


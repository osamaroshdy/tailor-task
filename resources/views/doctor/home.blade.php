@extends('layouts.doctors.app')

@section('content')
    <div class="container">
        @foreach(doctor()->notifications as $not)
        {{$not->markAsRead()}}
        <p class="alert alert-success">{{$not->data['message']}}</p>
           @php doctor()->readnotifications()->delete() @endphp
        @endforeach

        <h1>Reservations : {{ $reservations->total()}}</h1>
        <div class="card">
            <div class="card-body">
                @if ($reservations->count() > 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($reservations as $index=>$reservation)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $reservation->user->name }}</td>
                                @if($reservation->placement === 0)
                                    <td><p class="btn btn-sm btn-danger">{{ $reservation->status }}</p></td>
                                @elseif($reservation->placement === 1)
                                    <td><p class="btn btn-sm btn-success">{{ $reservation->status }}</p></td>
                                @else
                                    <td><p class="btn btn-sm btn-warning">{{ $reservation->status }}</p></td>
                                @endif
                                <td>
                                    <a href="{{ url('doctor/reservation/'. $reservation->id . '/edit') }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Show</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table><!-- end of table -->
                {{ $reservations->links() }}
                @else
                    <h2>No Data Found</h2>
                @endif

            </div><!-- end of box body -->


        </div><!-- end of box -->

    </div>
@endsection

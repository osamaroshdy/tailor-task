@extends('layouts.app')

@section('content')
    <div class="container">
        @auth
            @foreach(auth()->user()->notifications as $not)
                {{$not->markAsRead()}}
                <p class="alert alert-success">{{$not->data['message']}}</p>
                @php auth()->user()->readnotifications()->delete() @endphp
            @endforeach
        @endauth
        <div class="row">
            @foreach($doctors as $doctor)
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        <h4>{{ $doctor->name }} | {{ $doctor->specialty }}</h4>
                    </div>
                    <div class="card-body">
                        {{ $doctor->description }}
                    </div>
                    <div class="card-footer">
                        <a href="{{ url('/reservation', $doctor->id) }}" class="btn btn-success">Reservation</a>
                        <a href="{{ url('doctor/' . $doctor->id) }}" class="btn btn-info float-right">More Details</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection


@section('header')
    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
@endsection

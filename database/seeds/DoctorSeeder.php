<?php

use Illuminate\Database\Seeder;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Doctor::create([
            'name' => 'doctor',
            'email' => 'doctor@app.com',
            'password' => bcrypt(123456)
        ]);
    }
}

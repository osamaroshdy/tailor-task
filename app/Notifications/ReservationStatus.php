<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReservationStatus extends Notification
{
    use Queueable;

    public $status;
    public $name;
    public $message;
    public function __construct($status, $name)
    {
        $this->status = $status;
        $this->name = $name;
        $this->message = "Dear $name your reservation has been $status";
    }


    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase()
    {
        return [
            'message' => $this->message
        ];
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

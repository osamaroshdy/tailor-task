<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DoctorStatus extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $status;

    public function __construct($name, $status)
    {
        $this->name = $name;
        $this->status = $status;
    }

    public function build()
    {
        return $this->view('emails.doctors.status')->with(['name'=> $this->name, 'status' => $this->status]);
    }
}

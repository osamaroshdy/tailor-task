<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReservationStatus extends Mailable
{
    use Queueable, SerializesModels;


    public $name;
    public $status;
    public $date;


    public function __construct($name, $status, $date)
    {
        $this->name = $name;
        $this->status = $status;
        $this->date = $date;
    }


    public function build()
    {
        return $this->view('emails.reservations.status')->with(['name'=> $this->name, 'status' => $this->status, 'date' => $this->date]);
    }
}

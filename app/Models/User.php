<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;


    protected $fillable = [
        'name', 'email', 'password', 'is_banned'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getStatusAttribute()
    {
        if ($this->is_banned === 0) {
            return '';
        }

        return 'Banned';
    }


    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }


}

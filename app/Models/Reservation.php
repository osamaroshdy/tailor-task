<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['user_id', 'doctor_id', 'date', 'placement'];


    public function getStatusAttribute()
    {
        if ($this->placement === null) {
            return 'Pending';
        }

        if ($this->placement === 0) {
            return 'Rejected';
        }

        return 'Accepted ';
    }


    public function scopeAccepted($query)
    {
        return $query->where('placement', 1);
    }

    public function scopeRejected($query)
    {
        return $query->where('placement', 0);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }
}

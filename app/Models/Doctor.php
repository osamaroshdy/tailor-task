<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Doctor extends Authenticatable
{
    use Notifiable;


    protected $fillable = [
        'name', 'email', 'password', 'fees', 'location', 'description', 'specialty', 'placement'
    ];

    protected $hidden = [
        'password', 'remember_token', 'email_verified_at', 'created_at', 'updated_at'
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getStatusAttribute()
    {
        if ($this->placement === 0) {
            return 'Pending';
        }

        return 'Approved';
    }

    public function scopeApproved($query)
    {
        return $query->where('placement', 1);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}

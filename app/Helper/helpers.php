<?php


if (!function_exists('admin')) {
    function admin() {
        return auth()->guard('admin')->user();
    }
}


if (!function_exists('doctor')) {
    function doctor() {
        return auth()->guard('doctor')->user();
    }
}


if (!function_exists('aurl')) {
    function aurl($url){
        return url('dashboard/' . $url);
    }
}


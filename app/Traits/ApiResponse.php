<?php


namespace App\Traits;


trait ApiResponse
{

    private $paginate = 10;


    private function apiResponse($data = null, $error = null, $code = 200)
    {
        $array = [
          'data' => $data,
          'status' => in_array($code, $this->successCode()) == 200 ? true : false,
           'error' => $error
        ];

        return response($array, $code);
    }

    private function successCode()
    {
        return [
          200 , 201, 202
        ];
    }

}

<?php

namespace App\Http\Resources;

use App\Models\Reservation;
use App\Traits\ApiResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class DoctorResource extends JsonResource
{

    use ApiResponse;

    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'specialty' => $this->specialty,
            'fees' => $this->fees,
            'location' => $this->location,
            'description' => $this->description,
            'status' => $this->status,
            'reservations' => ReservationResource::collection($this->reservations()->paginate($this->paginate))
        ];
    }
}

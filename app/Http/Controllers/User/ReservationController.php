<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function store($id)
    {
        $doctor = Doctor::find($id);
        $doctor->reservations()->create([
                'user_id' => auth()->user()->id
        ]);

        return back();
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        $doctors = Doctor::approved()->get();
        return view('welcome', compact('doctors'));
    }

    public function showDoctor(Doctor $doctor)
    {
        return view('user.doctor', compact('doctor'));
    }
}

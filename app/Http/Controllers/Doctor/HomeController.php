<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Notifications\ReservationStatus;
use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        $reservations = doctor()->reservations()->paginate(20);
        return view('doctor.home', compact('reservations') );
    }

    public function editReservation(Reservation $reservation)
    {
        if($reservation->doctor_id === doctor()->id) {
            return view('doctor.show-reservation', compact('reservation') );
        }

        return abort(404);
    }

    public function UpdateReservation(Reservation $reservation, Request $request)
    {
        $reservation->placement = $request->placement;
        $reservation->save();

        $reservation->user->notify(new ReservationStatus($reservation->status,$reservation->user->name));
        Mail::to($reservation->user->email)->send(new \App\Mail\ReservationStatus($reservation->user->name, $reservation->status, $reservation->created_at));

        return redirect(url('doctor/home'));
    }
}

<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Doctor\Store;
use App\Models\Admin;
use App\Models\Doctor;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class DoctorAuth extends Controller
{


    public function register() {
        return view('doctor.register');
    }

    public function store(Store $request) {

        $data = $request->except(['password', 'password_confirmation']);
        $data['password'] = bcrypt($request->password);
        Doctor::create($data);
        return redirect(url('doctor/login'));
    }

    public function login() {
        return view('doctor.login');
    }

    public function dologin() {
        $rememberme = request('rememberme') === 1;

        if (auth()->guard('doctor')->attempt(['email' => request('email'), 'password' => request('password')], $rememberme)) {
            return redirect(RouteServiceProvider::DOCTOR_HOME);
        }

        session()->flash('error', 'These credentials do not match our records.');
        return back();
    }

    public function logout() {
        auth()->guard('doctor')->logout();
        return redirect(url('/'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class AdminAuth extends Controller
{
    public function login() {
        return view('admin.login');
    }

    public function dologin() {
        $rememberme = request('rememberme') === 1;

        if (auth()->guard('admin')->attempt(['email' => request('email'), 'password' => request('password')], $rememberme)) {
            return redirect(RouteServiceProvider::ADMIN_HOME);
        }

        session()->flash('error', 'These credentials do not match our records.');
        return back();
    }

    public function logout() {
        auth()->guard('admin')->logout();
        return redirect(url('dashboard/login'));
    }
}

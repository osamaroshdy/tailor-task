<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Store;
use App\Http\Requests\Admin\Update;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function index(Request $request)
    {
        $admins = Admin::where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('email', 'like', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('admin.admins.index', compact('admins'));
    }


    public function create()
    {
        return view('admin.admins.create');
    }

    public function store(Store $request)
    {

        $data = $request->except(['password', 'password_confirmation']);
        $data['password'] = bcrypt($request->password);
        Admin::create($data);
        toast('Admin has been added successfully','success','top-right')->hideCloseButton();
        return redirect(aurl('admin'));
    }

    public function edit(Admin $admin)
    {
        return view('admin.admins.edit', compact('admin'));
    }

    public function update(Update $request, Admin $admin)
    {
        $requestArray = $request->except('password');

        if(isset($requestArray['password']) && $requestArray['password'] !== ""){
            $requestArray['password'] =  Hash::make($requestArray['password']);
        }else{
            unset($requestArray['password']);
        }

        $admin->update($requestArray);
        toast('Admin has been updated successfully','success','top-right')->hideCloseButton();
        return redirect(aurl('admin'));
    }

    public function destroy(Admin $admin)
    {
        $admin->delete();
        toast(' Admin has been deleted successfully ','error','top-right')->hideCloseButton();
        return redirect(aurl('admin'));
    }
}

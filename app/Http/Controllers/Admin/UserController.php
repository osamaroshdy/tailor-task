<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('email', 'like', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('admin.users.index', compact('users'));
    }

    public function edit(User $user)
    {

        if ($user->is_banned === 0) {
            $user->is_banned = 1; // Ban User
            $user->save();
            toast('User has been banned successfully','success','top-right')->hideCloseButton();
            return back();
        }

        $user->is_banned = 0;  // unbanned user
        $user->save();
        toast('User has been unbanned successfully','success','top-right')->hideCloseButton();
        return back();
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));

    }
    public function destroy(User $user)
    {
        $user->delete();
        toast('User has been Deleted successfully','error','top-right')->hideCloseButton();
        return back();

    }
}

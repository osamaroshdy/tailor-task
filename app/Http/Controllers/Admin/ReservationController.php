<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReservationController extends Controller
{
    public function allReservation(Request $request)
    {
        $reservations = Reservation::latest()->paginate(10);

        return view('admin.reservations.index', compact('reservations'));
    }

    public function acceptedReservation(Doctor $doctor)
    {
        $reservations = Reservation::accepted()->paginate(10);

        return view('admin.reservations.index', compact('reservations'));
    }

    public function rejectedReservation(Doctor $doctor)
    {
        $reservations = Reservation::rejected()->paginate(10);

        return view('admin.reservations.index', compact('reservations'));
    }

    public function reportReservation()
    {

        $reservations = Reservation::select(
          DB::raw('YEAR(created_at) AS YEAR'),
          DB::raw('MONTH(created_at) AS MONTH'),
          DB::raw('DAY(created_at) AS DAY'),
          DB::raw('COUNT(created_at) AS COUNT')
        )->groupBy('day')->get();

        return view('admin.reservations.report', compact('reservations'));
    }

}

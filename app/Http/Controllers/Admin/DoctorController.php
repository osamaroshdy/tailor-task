<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Notifications\DoctorPlacement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DoctorController extends Controller
{
    public function index(Request $request)
    {
        $doctors = Doctor::where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('email', 'like', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('admin.doctors.index', compact('doctors'));
    }

    public function edit(Doctor $doctor)
    {
        if ($doctor->placement === 0) {
            $doctor->placement = 1; // Approved doctor
            $doctor->save();

            Mail::to($doctor->email)->send(new \App\Mail\DoctorStatus($doctor->name, $doctor->status));
            $doctor->notify(new DoctorPlacement($doctor->status,$doctor->name));
            toast('Doctor has been Approved successfully','success','top-right')->hideCloseButton();
            return back();
        }

        $doctor->placement = 0;  // pending doctor
        $doctor->save();

        Mail::to($doctor->email)->send(new \App\Mail\DoctorStatus($doctor->name, $doctor->status));
        $doctor->notify(new DoctorPlacement($doctor->status,$doctor->name));
        toast('Doctor has been Pending successfully','success','top-right')->hideCloseButton();
        return back();
    }

    public function show(Doctor $doctor)
    {
        return view('admin.doctors.show', compact('doctor'));
    }

    public function destroy(Doctor $doctor)
    {
        $doctor->delete();
        toast('Doctor has been Deleted successfully','error','top-right')->hideCloseButton();
        return back();

    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DoctorResource;
use App\Models\Doctor;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    use ApiResponse;


    public function index()
    {
        $data = DoctorResource::collection(Doctor::paginate($this->paginate));

        return $this->apiResponse($data, null, 202);

    }

    public function show($id)
    {
        $doctor = Doctor::find($id);

        if ($doctor) {
            return $this->apiResponse(new DoctorResource($doctor));

        }

        return $this->apiResponse(null, 'No Doctor Found', 404);

    }
}

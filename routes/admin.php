<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::prefix('dashboard')->name('dashboard.')->group(function () {


    Route::get('/login', 'AdminAuth@login');
    Route::post('/login', 'AdminAuth@doLogin');
    Config::set('auth.defines', 'admin');

    Route::group(['middleware' => 'admin:admin'], function () {

        Route::get('/', 'HomeController@index');

        // admin routes
        Route::resource('admin', 'AdminController');

        // doctor routes
        Route::resource('doctor', 'DoctorController')->except(['create','store']);

        // user routes
        Route::resource('user', 'UserController')->except(['create','store']);

        // reservation routes
        Route::get('reservation', 'ReservationController@allReservation');
        Route::get('reservation/accepted', 'ReservationController@acceptedReservation');
        Route::get('reservation/rejected', 'ReservationController@rejectedReservation');
        Route::get('reservation/report', 'ReservationController@reportReservation');

        Route::post('/logout', 'AdminAuth@logout');

    });


});//end of dashboard routes

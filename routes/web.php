<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();


Route::namespace('User')->group(function (){

    Route::get('/', 'WelcomeController@index')->name('home');

//    Route::get('/doctor/{doctor}', 'WelcomeController@showDoctor');

    Route::get('/reservation/{id}', 'ReservationController@store')->middleware('auth');


});



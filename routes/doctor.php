<?php


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;


Route::prefix('doctor')->name('doctor.')->group(function () {

    Route::get('/login', 'DoctorAuth@login')->name('login');
    Route::post('/login', 'DoctorAuth@doLogin');
    Route::get('/register', 'DoctorAuth@register')->name('register');
    Route::post('/register', 'DoctorAuth@store');
    Config::set('auth.defines', 'doctor');

    Route::group(['middleware' => 'doctor:doctor'], function () {

        Route::get('/home', 'HomeController@index');

        Route::get('/reservation/{reservation}/edit', 'HomeController@editReservation');
        Route::put('/reservation/{reservation}', 'HomeController@updateReservation')->name('reservation.update');

        Route::post('/logout', 'DoctorAuth@logout');

    });


});//end of dashboard routes
